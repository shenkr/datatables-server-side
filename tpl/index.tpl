<div class="row">
    <div class="col-sm-12">
        <table id="employees" class="table table-condensed table-striped table-hover table-bordered display compact">
            <thead>
                <tr>
					<th></th>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <button id="download" type="button" class="btn btn-success xlsx">Download as .xlsx</button>
		<button id="mail" data-id="xlsx" type="button" class="btn btn-success xlsx">Send .xlsx to Mail</button>
		<button id="download" type="button" class="btn btn-primary docx">Download as .docx</button>
		<button id="mail" data-id="docx" type="button" class="btn btn-primary docx">Send .docx to Mail</button>
		<button id="download" type="button" class="btn btn-danger pdf">Download as .pdf</button>
		<button id="mail" data-id="pdf" type="button" class="btn btn-danger pdf">Send .pdf to Mail</button>
    </div>
</div>