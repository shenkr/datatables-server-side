<?php

class DB {

    private $db;
    public $user;

    function __construct() {
        // opening db connection
        $this->pdo = new PDO("mysql:dbname=" . Config::$DB_NAME, Config::$DB_USERNAME, Config::$DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->fpdo = new FluentPDO($this->pdo);

        $this->fpdo->debug = function($BaseQuery) {
            echo "query: " . $BaseQuery->getQuery(false) . "\n";
            echo "parameters: " . implode(', ', $BaseQuery->getParameters()) . "\n";
            echo "full query:" . vsprintf(str_replace("?", "\"%s\"", $BaseQuery->getQuery(false)), $BaseQuery->getParameters());
            echo "rowCount: " . $BaseQuery->getResult()->rowCount() . "\n";
            // time is impossible to test (each time is other)
            echo $BaseQuery->getTime() . "\n";
        };
        $this->fpdo->debug = null;
    }

    /**
     * Select all employees
     * @return array|string $result - array of employees
     */
    public function getSelectData () {

        $query = $this
            ->fpdo
            ->from('employees')
            ->select(null)
            ->select('emp_no, first_name, last_name, birth_date');

        $result['data'] = $query->fetchAll();

        return $result;
    }

    /**
     * Select all employees
     * @param string       $string  The prefix
     * @return number       $result - number of employees
     */
    public function rowCount($column)
    {
        $query = $this
            ->fpdo
            ->from('employees')
            ->select('emp_no, first_name, last_name, birth_date');

        $query->execute();
        return $query->getResult()->rowCount();
    }

    /**
     * Select all employees
     * @param string       $params  The prefix
     * @return array       $result - array of employees
     */
    public function getSelectDataByParams ($search, $offset, $orderBy, $limit) {
        
        $query = $this
            ->fpdo
            ->from('employees')
            ->select('emp_no, first_name, last_name, birth_date')
            ->where
            (
                '(emp_no LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR birth_date LIKE ?)',
                '%' . $search . '%',
                '%' . $search . '%',
                '%' . $search . '%',
                '%' . $search . '%'
            )
            ->offset($offset)
            ->orderBy($orderBy);

        $query->execute();
        $query->limit($limit);

		//ware
        $result['data'] = $query->fetchAll();

        return $result;
    }

    

}