<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function echoResponse ($code, $response = array()) {

    $http_status_codes = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        419 => 'Authentication Timeout',
        420 => 'Enhance Your Calm',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        424 => 'Method Failure',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        444 => 'No Response',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        451 => 'Unavailable For Legal Reasons',
        494 => 'Request Header Too Large',
        495 => 'Cert Error',
        496 => 'No Cert',
        497 => 'HTTP to HTTPS',
        499 => 'Client Closed Request',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
        598 => 'Network read timeout error',
        599 => 'Network connect timeout error'
    );

    header('Content-Type: text/html;charset=UTF-8');
    header("HTTP/1.0 $code $http_status_codes[$code]");

    echo json_encode($response, JSON_UNESCAPED_UNICODE);

    die();
}

function getSelectData ($request, $slim_response, $args) {

    $db = new DB();

    $data = $db->getSelectData();

    echoResponse(200, $data);
}

function getSelectDataByParams ($request, $slim_response, $args) {

    $db = new DB();

    $limit = $request->getParam('length');
    $offset = $request->getParam('start');
    $orderBy = $request->getParam('columns')[$request->getParam('order')[0]['column']]['data'].' '. $request->getParam('order')[0]['dir'];
    $search = $request->getParam('search')['value'];
    $data = $db->getSelectDataByParams($search, $offset, $orderBy, $limit);

    $data['draw'] = $request->getParam('draw');
    $data['recordsTotal'] = $db->rowCount('employees');
    $data['recordsFiltered'] = $db->rowCount('employees');


    echoResponse(200, $data);
}
// function name space
function getXlsx ($request, $slim_response, $args) {

    $excel = new PHPExcel();

    $db = new DB();

    $limit = $request->getParam('length');
    $offset = $request->getParam('start');
    $orderBy = $request->getParam('columns')[$request->getParam('order')[0]['column']]['data'] .' '. $request->getParam('order')[0]['dir'];
    $search = $request->getParam('search')['value'];

    $data = $db->getSelectDataByParams($search, $offset, $orderBy, $limit);

    $excel->getActiveSheet()->setCellValue('A1', '#');
    $excel->getActiveSheet()->setCellValue('B1', 'First Name');
    $excel->getActiveSheet()->setCellValue('C1', 'Last Name');
    $excel->getActiveSheet()->setCellValue('D1', 'Birth Date');

    $excel_offset = 2;

    for($i = 0; $i < $limit; $i++) {
        $excel->getActiveSheet()->setCellValue('A' . ($i + $excel_offset), $data['data'][$i]['emp_no']);
        $excel->getActiveSheet()->setCellValue('B' . ($i + $excel_offset), $data['data'][$i]['first_name']);
        $excel->getActiveSheet()->setCellValue('C' . ($i + $excel_offset), $data['data'][$i]['last_name']);
        $excel->getActiveSheet()->setCellValue('D' . ($i + $excel_offset), $data['data'][$i]['birth_date']);
    }
    

    $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $filename = uniqid('table_') . '.xlsx';
    $dir = $_SERVER["DOCUMENT_ROOT"] . '/temp/';
    
    $writer->save($dir . $filename);
	
    $data = $filename;

	if($request->getParam('mail')) {
		sendMail('bohdan.jax@gmail.com', 'bohdan.jax@gmail.com', 'Xlsx Table', $dir, $filename);
	}

	echoResponse(200, $data);

    
}

function sendMail ($from, $to, $body, $filedir, $filename) {
	$mail = new PHPMailer;

	$mail->SMTPDebug = 3;
	$mail->isSMTP();                        
	$mail->Host = 'smtp.gmail.com';
	$mail->SMTPAuth = true;   
	$mail->Username = 'bohdan.jax@gmail.com';                 
	$mail->Password = '58362490Bb';
	$mail->SMTPSecure = 'tls';                           
	$mail->Port = 587;                                   

	$mail->From = $from;
	$mail->FromName = 'Full Name';

	$mail->addAddress($to, 'Office');

	$mail->isHTML(true);
	$mail->Subject = $body;
	$mail->Body = '<i>' . $body . '</i>';

	$mail->addAttachment($filedir . $filename, $filename);
	
	$mail->send();
}

function getPdf ($request, $slim_response, $args) {
	$db = new DB();

    $limit = $request->getParam('length');
    $offset = $request->getParam('start');
    $orderBy = $request->getParam('columns')[$request->getParam('order')[0]['column']]['data'] .' '. $request->getParam('order')[0]['dir'];
    $search = $request->getParam('search')['value'];

    $data = $db->getSelectDataByParams($search, $offset, $orderBy, $limit);
	
	$pdf = new FPDF();
	$pdf->AddPage();
	$width_cell=array(20,50,40,40);
	$pdf->SetFont('Arial', 'B', 14);

	$pdf->Cell($width_cell[0], 10, '#', 1, 0);
	$pdf->Cell($width_cell[1], 10, 'First Name', 1, 0);
	$pdf->Cell($width_cell[2], 10, 'Last Name', 1, 0);
	$pdf->Cell($width_cell[3], 10, 'Birth Date', 1, 0);
	$pdf->Ln();
	
	for($i = 0; $i < $limit; $i++) {
		$pdf->Cell($width_cell[0], 10, $data['data'][$i]['emp_no'], 1, 0);
		$pdf->Cell($width_cell[1], 10, $data['data'][$i]['first_name'], 1, 0);
		$pdf->Cell($width_cell[2], 10, $data['data'][$i]['last_name'], 1, 0); 
		$pdf->Cell($width_cell[3], 10, $data['data'][$i]['birth_date'], 1, 0);
		$pdf->Ln();
    }
	
	$filename = uniqid('table_') . '.pdf';
    $dir = $_SERVER["DOCUMENT_ROOT"] . '/temp/';

	$pdf->Output($dir . $filename, 'F');
	
	$data = $filename;
	
	if($request->getParam('mail')) {
		sendMail('bohdan.jax@gmail.com', 'bohdan.jax@gmail.com', 'Pdf Document', $dir, $filename);
	}
	
	echoResponse(200, $data);
}

function getDocx ($request, $slim_response, $args) {
	

	$db = new DB();

    $limit = $request->getParam('length');
    $offset = $request->getParam('start');
    $orderBy = $request->getParam('columns')[$request->getParam('order')[0]['column']]['data'] .' '. $request->getParam('order')[0]['dir'];
    $search = $request->getParam('search')['value'];

    $data = $db->getSelectDataByParams($search, $offset, $orderBy, $limit);
	
	$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('../tpl/template.docx');
	
	$templateProcessor->cloneRow('emp_no', $limit);
	
	for($i = 1; $i <= $limit; $i++) {
		$templateProcessor->setValue('emp_no#' . $i, $data['data'][$i - 1]['emp_no']);
		$templateProcessor->setValue('first_name#' . $i, $data['data'][$i - 1]['first_name']);
		$templateProcessor->setValue('last_name#' . $i, $data['data'][$i - 1]['last_name']);
		$templateProcessor->setValue('birth_date#' . $i, $data['data'][$i - 1]['birth_date']);
    }

	
	$filename = uniqid('table_') . '.docx';
    $dir = $_SERVER["DOCUMENT_ROOT"] . '/temp/';

	$templateProcessor->saveAs($dir . $filename);
	
	$data = $filename;
	
	if($request->getParam('mail')) {
		sendMail('bohdan.jax@gmail.com', 'bohdan.jax@gmail.com', 'Docx Document', $dir, $filename);
	}
	
	echoResponse(200, $data);
}