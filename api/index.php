<?php

require_once 'config.php';
require_once 'DB.php';
require_once 'functions.php';


require '../lib/php/vendor/autoload.php';
require '../lib/php/vendor/PHPExcel/Classes/PHPExcel.php';


//Create a new PHPMailer instance

require '../lib/php/vendor/PHPMailer/src/Exception.php';
require '../lib/php/vendor/PHPMailer/src/PHPMailer.php';
require '../lib/php/vendor/PHPMailer/src/SMTP.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$app->post('/data', 'getSelectDataByParams');

$app->post('/xlsx', 'getXlsx');
$app->post('/docx', 'getDocx');
$app->post('/pdf', 'getPdf');

$app->run();
