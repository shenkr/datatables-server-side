(function() {

    var context;
    var app = Sammy.apps.body;

    app.get('#/', function() {

        context = this;

        context.render(
            './tpl/index.tpl',
            {},
            function(output) {
                document.title = 'Сотрудники';
                $('#container').html(output);
				
				

                var table = $('#employees').DataTable( {
                    "drawCallback": function() {

                        table.cells().every( function () {
							$(this.node()).not('.details-control').click(function() { alert($(this).text()) }) ;
                        });
                    },
                    "initComplete": function(settings, json) {
						
                        $("#download.xlsx").click(() => {
							var params = table.ajax.params();
                            //post
                            $.ajax({
                                method: 'POST',
                                url: './api/xlsx',
                                dataType: "json",
                                data: {
                                    "columns": params.columns,
                                    "length": params.length,
                                    "order": params.order,
                                    "search": params.search,
                                    "start": params.start
                                },
                                success: (data) => {
                                    fetch(document.location.origin + '/temp/' + data)
                                        .then(res => res.blob())
                                        .then(blob => {
                                            saveAs(blob, data);
                                        });
                                }
                            })
                            
                        });
						$("#mail.xlsx").click(() => {
							var params = table.ajax.params();
                            //post
                            $.ajax({
                                method: 'POST',
                                url: './api/xlsx',
                                dataType: "json",
                                data: {
                                    "columns": params.columns,
                                    "length": params.length,
                                    "order": params.order,
                                    "search": params.search,
                                    "start": params.start,
									"mail": 'bohdan.jax@gmail.com'
                                }
                            })
                        });
						$("#mail.docx").click(() => {
							var params = table.ajax.params();
                            //post
                            $.ajax({
                                method: 'POST',
                                url: './api/docx',
                                dataType: "json",
                                data: {
                                    "columns": params.columns,
                                    "length": params.length,
                                    "order": params.order,
                                    "search": params.search,
                                    "start": params.start,
									"mail": 'bohdan.jax@gmail.com'
                                }
                            })
                        });
						$("#download.docx").click(() => {
							var params = table.ajax.params();
                            //post
                            $.ajax({
                                method: 'POST',
                                url: './api/docx',
                                dataType: "json",
                                data: {
                                    "columns": params.columns,
                                    "length": params.length,
                                    "order": params.order,
                                    "search": params.search,
                                    "start": params.start
                                }
                            })
                        });
						$("#mail.pdf").click(() => {
							var params = table.ajax.params();
                            //post
                            $.ajax({
                                method: 'POST',
                                url: './api/pdf',
                                dataType: "json",
                                data: {
                                    "columns": params.columns,
                                    "length": params.length,
                                    "order": params.order,
                                    "search": params.search,
                                    "start": params.start,
									"mail": 'bohdan.jax@gmail.com'
                                }
                            })
                        });
						$("#download.pdf").click(() => {
							var params = table.ajax.params();
                            //post
                            $.ajax({
                                method: 'POST',
                                url: './api/pdf',
                                dataType: "json",
                                data: {
                                    "columns": params.columns,
                                    "length": params.length,
                                    "order": params.order,
                                    "search": params.search,
                                    "start": params.start
                                }
                            })
                        });
                    },
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "./api/data",
                        "type": "POST"
                    },
                    "columns": [
						{
							"className":      'details-control',
							"orderable":      false,
							"data":           null,
							"defaultContent": ''
						},
                        { "data": "emp_no" },
                        { "data": "first_name" },
                        { "data": "last_name" }
                    ],
					"order": [[ 1, "asc" ]]
                });
				
				function format (date) {
					return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
						'<tr>'+
							'<td>Birth Date:</td>'+
							'<td>'+ date.birth_date +'</td>'+
						'</tr>'+
					'</table>';
				}
				
				$('#employees tbody').on('click', 'td.details-control', function () {
					var tr = $(this).closest('tr');
					var row = table.row(tr);
			 
					if ( row.child.isShown() ) {
						row.child.hide();
						tr.removeClass('shown');
					}
					else {
						row.child(format(row.data())).show();
						tr.addClass('shown');
					}
				});
            }
        );
    });
    
    
})();